// REACT \\
import React, {useState, useEffect} from 'react';
import {View, TouchableHighlight, Text, RefreshControl, SafeAreaView, ScrollView, StyleSheet, Alert, PermissionsAndroid} from 'react-native';

// NAVIGATION \\
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// FONT AWESOME \\
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

// ASYNC STORAGE \\
import AsyncStorage from '@react-native-async-storage/async-storage';

// GEO FENCING \\
import Boundary, {Events} from 'react-native-boundary';

// GEOLOCATION \\
import Geolocation from '@react-native-community/geolocation';

// LOKALNI IMPORTI \\
import Location from './Components/Location';
import UnosLokacije from './Components/UnosLokacije';
import ManualnoUpravljanje from './Components/ManualnoUpravljanje';
//import { whileStatement } from '@babel/types';

const Stack = createNativeStackNavigator();
// const wait = (timeout) => {
//   return new Promise(resolve => setTimeout(resolve, timeout));
// }

const App = () => {

  const [refreshing, setRefreshing] = React.useState(false);
  const [Status, setStatus] = useState({"Vrata" : "loading"},{"AutomatikaVrata" : "loading"});
  const [locations, setLocations] = useState([]);

  const [longitude, setLongitude] = useState();
  const [latitude, setLatitude] = useState();

  React.useEffect(() => {
    // compomentWillMount
    getLocations();

    // LOCATION PERMISSION
    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
        subscribeLocationLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            console.error('Access denied!');
          }
        } catch (err) {
          console.error(err);
        }
      }
    };
    requestLocationPermission();

    // SETUP BOUNDARY EVENTS
    Boundary.on(Events.ENTER, id => {
      Alert.alert(
        "Otvaranje vrata",
        "id: " + id,
        [
          {
            text: "Nemoj!",
            style: "cancel"
          },
          {
            text: "Moj!",
            onPress: () => {
              // NASTAVI DALJE \\
            } 
          }
        ]
      );
    });

    Boundary.on(Events.EXIT, id => {
      Alert.alert("Izasao!", "ID: " + id);
    });

    return () => {
      Geolocation.clearWatch(watchID);

      // componentWillUnmount
      Boundary.off(Events.ENTER);
      Boundary.off(Events.EXIT);
      Boundary.removeAll();
    }
  }, []);
  
  const getOneTimeLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        setLongitude(JSON.stringify(position.coords.longitude));
        setLatitude(JSON.stringify(position.coords.latitude));
      },
      (e) => console.error(e),
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 1000
      },
    );
  };

  const subscribeLocationLocation = () => {
    watchID = Geolocation.watchPosition(
      (position) => {
        setLongitude(JSON.stringify(position.coords.longitude));
        setLatitude(JSON.stringify(position.coords.latitude));
      },
      (e) => console.error(e),
      {
        enableHighAccuracy: false,
        maximumAge: 1000
      }
    );
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    GetStatusAndSetState();
  }, []);

  function GetStatusAndSetState(json) {
    console.log("fetching status");
    fetch('http://213.202.124.25/Json')
    .then((response) => response.json())    
    .then((json) => {
      setStatus(json);
      setRefreshing(false);
      console.log("fetched status");
    })
    .catch((error) => {
      console.error(error);
    })
  };

  // funkcije lokacija \\
  React.useEffect(() => {
    Boundary.removeAll();
    locations.forEach(x => {
      Boundary.add({
        lat: parseFloat(x.latitude),
        lng: parseFloat(x.longitude),
        radius: 50, // in meters
        id: x.id,
      })
      .then(() => console.log("Dodan " + x.id))
      .catch(e => console.error(e));
    });
  }, [locations])

  const getLocations = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('locations')
      setLocations(jsonValue != null ? JSON.parse(jsonValue) : []);
    } catch(e) {
      console.log(e);
    }
  };

  const addLocation = async (location) => {
    try {
      await AsyncStorage.setItem('locations', JSON.stringify([...locations, location]));
      getLocations();
    } catch (e) {
      console.error(e);
    }
  };

  const deleteLocation = async (id) => {
    try {
      await AsyncStorage.setItem('locations', JSON.stringify(locations.filter(x => x.id !== id)));
      getLocations();
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="MainScreen" options={{headerShown: false}}>
          {props => 
              <ScrollView
              
                contentContainerStyle={styles.scrollView}
                refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
              >
                <ManualnoUpravljanje {...props} statusHandle={setStatus.bind(this)} status={Status}  />
                <Text style={{fontWeight:'bold', margin: 16}}>LOKACIJE</Text>
                
                {locations.map(item => {
                  return (
                    <Location  key={item.id} location={item} onDelete={deleteLocation} />
                  );
                })}

                <TouchableHighlight underlayColor="#0069d9" onPress={() => {props.navigation.navigate('AddLocationScreen')}} style={{shadowColor: "#000", shadowOffset: {width: 0,height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, width: 212, height: 60, borderRadius: 30, backgroundColor: '#007bff', position: 'absolute', bottom: 10, right: 10, padding: 18}}>
                  <View style={{flexDirection: 'row'}}>
                    <FontAwesomeIcon icon={ faPlus } size={24} color={'white'} />
                    <Text style={{paddingLeft: 14, color:'white', fontWeight: 'bold', fontSize: 16}}>DODAJ LOKACIJU</Text>
                  </View>
                </TouchableHighlight>
              </ScrollView>
          }
        </Stack.Screen>
        <Stack.Screen name="AddLocationScreen">
          {props => <UnosLokacije {...props} addLocation={addLocation} longitude={longitude} latitude={latitude} updatePosition={getOneTimeLocation} />}
        </Stack.Screen>
      </Stack.Navigator>
     
    </NavigationContainer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1
  },
});

export default App;
