import React, { useState, useEffect } from "react";
import { ActivityIndicator, Text, View, Button } from 'react-native';

const ManualnoUpravljanje = ({navigation, statusHandle, status}) => {

    const [loading, setLoading] = useState(true);

    useEffect(() => {     
        GetStatusAndSetState();
    },[]);

    if(loading){
        return(
        <View>
            <ActivityIndicator size="large" color="#0000ff" style={{paddingTop:32, paddingBottom: 23  }} />
        </View>
       )
    }

    function GetStatusAndSetState() {
        console.log("fetching status");
        setLoading(true);
        fetch('http://213.202.124.25/Json')
        .then((response) => response.json())    
        .then((json) => {
            statusHandle(json);
            setLoading(false);
        })
        .catch((error) => {
            console.error(error);
            setLoading(false);
        })
    
    };

    const send = (cmd) => {
        setLoading(true);
        fetch(`http://213.202.124.25/${cmd}`)
        .then(() => GetStatusAndSetState());
    };

    return (
        <View style={{margin: 16}} >
            <Text style={{fontWeight:'bold', marginTop: 16}}>MANUALNO UPRAVLJANJE</Text>
            <View style={{flexDirection:'row',  marginTop: 16, alignItems:'center'}}>
                <Text style={{flex:1}}>Status vrata: </Text>
                <Text style={{fontWeight:'bold', flex:1}} > {status.Vrata.toUpperCase()} </Text>
                <Button 
                    style={{flex:1}}
                    onPress={() => send('RelayOnOff')}
                    title={status.Vrata == 'Otvorena' ? 'Zatvori' : 'Otvori'}
                    color={status.Vrata == 'Otvorena' ? '#FF0000' : 'fireRed'}
                />
            </View>

            <View style={{flexDirection:'row', marginTop:8, justifyContent:"center", alignItems:'center' }} > 
                <Text style={{flex:1}} >Automatika: </Text> 
                <Text style={{fontWeight:'bold', flex:1}}> {status.AutomatikaVrata == 2 ? 'ISKLJUČENA' : 'UKLJUČENA'} </Text>
                <Button 
                    style={{flex:1}}
                    onPress={() => status.AutomatikaVrata == 2 ? send('AutomatikaOn') : send('AutomatikaOff')}
                    title={status.AutomatikaVrata == 1 ? 'Isključi' : 'Uključi'}
                    color={status.AutomatikaVrata == 1 ? '#FF0000' : '#008000'}
                />
                
            </View>
        </View>
    );
}



export default ManualnoUpravljanje
