import React from 'react';
import {View, Text, TouchableHighlight, StyleSheet} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

const Location = ({location, onDelete}) => {
    return (
      <View style={{flexDirection: 'row', height:72 }}>
        <View style={{padding: 16, flex:5}}>
          <Text style={{color: 'black'}} >{location.name}</Text>
          <View style={{flexDirection: 'row', paddingTop: 8}}>
            <Text style={{fontSize: 12}}>Lng: {location.longitude}</Text>
            <Text style={{fontSize: 12, paddingLeft: 16}}>Lat: {location.latitude}</Text>
          </View>
        </View>
        <TouchableHighlight  onPress={() => onDelete(location.id)} style={{height: '100%', aspectRatio: 1, padding: 20}}>
            <FontAwesomeIcon icon={ faTrash } size={24} color={'red'} />
        </TouchableHighlight>
      </View>
    )
}

export default Location

