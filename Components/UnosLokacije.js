import React, {useState, useEffect} from 'react';
import {View, Button, Text, TextInput} from 'react-native';

//import Geolocation from '@react-native-community/geolocation';
import uuid from 'react-native-uuid';

function UnosLokacije({navigation, addLocation, latitude, longitude}) {

    const [name, onChangeName] = useState();
    // const [currentLongitude, setCurrentLongitude] = useState('');
    // const [currentLatitude, setCurrentLatitude] = useState('');
    
    // useEffect(() => {
    //   const requestLocationPermission = async () => {
    //     if (Platform.OS === 'ios') {
    //       getOneTimeLocation();
    //       subscribeLocationLocation();
    //     } else {
    //       try {
    //         const granted = await PermissionsAndroid.request(
    //           PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    //           {
    //             title: 'Location Access Required',
    //             message: 'This App needs to Access your location',
    //           },
    //         );
    //         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //           //To Check, If Permission is granted
    //           getOneTimeLocation();
    //         } else {
    //           console.log('Location permission denied');
    //         }
    //       } catch (err) {
    //         console.warn(err);
    //       }
    //     }
    //   };
    //   requestLocationPermission();
    //   return () => {
    //     Geolocation.clearWatch(watchID);
    //   };
    // }, []);

    // const getOneTimeLocation = () => {
    //   Geolocation.getCurrentPosition(
    //     //Will give you the current location
    //     (position) => {

    //       //Setting Longitude state
    //       setCurrentLongitude(JSON.stringify(position.coords.longitude));
          
    //       //Setting Longitude state
    //       setCurrentLatitude(JSON.stringify(position.coords.latitude));
    //     },
    //     (error) => {
    //       console.log(error.message);
    //     },
    //     {
    //       enableHighAccuracy: false,
    //       timeout: 30000,
    //       maximumAge: 1000
    //     },
    //   );
    // };

    const saveLocation = async () => {
        addLocation({
            id: uuid.v4(),
            name: name,
            longitude: longitude,
            latitude: latitude
        });

        navigation.goBack();
      };

    return (
        <View>
            <TextInput
                placeholder="Ime lokacije"
                onChangeText={onChangeName}
                value={name}
            />

            <Text
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 16,
              }}>
              Longitude: {longitude}
            </Text>
            <Text
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 16,
              }}>
              Latitude: {latitude}
            </Text>
            <View style={{marginTop: 20}}>
              {/* <Button
                title="Dohvati trenutnu lokaciju"
                onPress={updatePosition}
              /> */}
            </View>

            <Button title="Dodaj lokaciju" onPress={saveLocation} />

            
        </View>
    )
}

export default UnosLokacije

