import React, {useState} from 'react'
import { View, Text, Button, FlatList, StyleSheet, TouchableHighlight, Alert } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import AsyncStorage from '@react-native-async-storage/async-storage';

import UnosLokacije from './UnosLokacije';
import Location from './Location';

// GEO FENCING \\
import Boundary, {Events} from 'react-native-boundary';


const Locations = ({navigation, locations, onDelete}) => {

  React.useEffect(() => {

    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        // za ios \\
      } else {
        try {
          const backgroundgranted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
            {
              title: 'Background Location Permission',
              message:
                'We need access to your location ' +
                'so you can get live quality updates.',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (backgroundgranted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
          } else {
            console.log('Location permission denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
    };
    requestLocationPermission();

    Boundary.off(Events.ENTER);
    

    Boundary.off(Events.EXIT);
    
  },[]);

  return (
    <View>
      
    </View>
  )
}

export default Locations;                                           
